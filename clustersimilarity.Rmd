# Cluster similarity analysis {#clustersimilarity}

```{r, out.width = '100%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/page.png")
```

Integrative data analysis approaches have been successfully used to analyze multiple datasets simultaneously, to compare the results of independent experiments: this strategy allows to perform a deeper interpretation of the data than it is possible without. This approach can also be used to investigate which clusters/groups are similar (or different) between each other by comparing the feature signatures of these clusters/groups.

To perform this kind of integrative analysis, we use the Jaccard similarity index, to identify similar clusters/groups across studies or conditions.

BIOMEX performs the cluster similarity analysis by combining the results obtained during marker set analysis (Section \@ref(markersetanalysis)). The output of this analysis is a similarity score, which describes (quantitatively) how each group is similar to the other groups. In addition, for each cluster/group, it defines which features are common and specific compared to all the other groups.

How the cluster similarity analysis is performed is explained in Section \@ref(clustersimilarityexplanation).

```{block, type = 'rmdnote'}
The cluster similarity analysis can be performed only if you saved at least two marker set analysis results.
```

## Settings

```{r, out.width = '50%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/settings.png")
```

In the *Settings* box you will be able to define how to perform the similarity analysis. First, you need to define the number of top ranked features to use in *Method settings*, and then you can select the comparisons to include in the similarity analysis in *Data selection settings*.

### Method settings

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/methodsettings.png")
```

When you click on *Method settings*, you can start to define how many top ranked features to use for each cluster/group. You can set the following parameters:

- **Number of (top) features to use**: it indicates the number of top ranked features to use in the analysis. It will take the features starting from rank 1 to the rank of the selected number.
- **Apply upper case to features**: all the feature names will be converted to upper case. This can be useful when different annotation styles are used for different datasets (e.g. human genes are all upper case, mouse genes have the first character upper case, all the others lower case).

The selection of the number of the top ranked features to use in the cluster similarity analysis is extremely important, since you need to select a number that defines the nature of your cluster well. For example, using the top 10 features of your cluster could not define its biological function in the best possible way, while by using the top 60 features you could start to introduce some unwanted noise.

```{block, type = 'rmdtip'}
There is no rule on which number of top ranked features to use is the best number. Usually values between 50 and 100 seem to work well.
```

### Data selection settings

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/dataselectionsettings.png")
```

When you click on *Data selection settings*, you can start to select the comparisons to include in the cluster similarity analysis. The comparisons you can select are the ones you explicitly saved during the marker set analysis (see Section \@ref(msasaveresults)). 

You can set the following parameters:

- **Data to select**: the comparisons you want to include in the analysis.
- **Group to remove**: the clusters/groups you want to remove from a specific comparison.
- **Class to remove**: the clusters/groups you want to remove from all comparisons.

Every time you select a comparison, some information will be displayed about the comparison:

- **Name**: the name of the comparison.
- **Experiment**: the name of the experiment in which the comparison was executed (see Section \@ref(manageexperiments)).
- **Variable**: the name of the metadata variable used to perform the marker set analysis.
- **Feature**: the features used as input for the comparison (these are the features you selected in the design of experiment).
- **Group**: the groups used.
- **Covariate**: the covariates used.
- **Analysis**: the type of analysis performed.
- **Matrix**: the matrix used as input for the comparison (either the normal matrix or the engineered matrix).

You can also permanently delete all the selected comparisons by clicking on the *Delete selected entries* button.

```{r, out.width = '100%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/dataselectiontable.png")
```

### Performing the meta analysis

When the parameters are all set-up, you can click on the *Update* button to compute the cluster similarity results.

As soon as the similarity analysis is computed, the results will be ready to be explored. There are different types of results available: the dimensionality reduction results, the congruent features results and the Venn diagrams. How these results are computed is explained in Section \@ref(clustersimilarityexplanation).

The results to display can be changed by clicking on *Result settings* in the Customization box.

#### Dimensionality reduction results

Dimensionality reduction is performed on the Jaccard similarity matrix. Specifically, principal component analysis (PCA) is employed to perform the dimensionality reduction. For more details about the dimensionality reduction, you can read Section \@ref(dimensionalityreduction).

If *Plot* is selected in the top left corner of the main box, the dimensionality reduction plot will be shown, otherwise if *Table* is selected, the Jaccard similarity matrix used to perform the dimensionality reduction will be displayed.

The dimensionality reduction plot shows you how each cluster/group is similar (or different) with respect to all the other clusters/groups. On the plot, the colors represent the clusters/groups, while the symbols represent the comparisons. 

```{r, out.width = '100%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/dimensionalityreductionplotresults.png")
```

The Jaccard similarity matrix looks like a correlation matrix, where each cluster/group is tested against all other clusters/groups to compute the Jaccard similarity index. 

```{r, out.width = '100%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/dimensionalityreductiontableresults.png")
```

### Congruent features results

For each cluster/group, we define which features are common and specific for that cluster/group. To do this, for each cluster/group we calculate the occurrence of each feature (how many times the feature appears in the cluster/group across all comparisons), and the median rank of each feature.

If *Plot* is selected in the top left corner of the main box, the congruent features plot will be shown, otherwise if *Table* is selected, the congruent features table will be displayed.

The congruent features plot is displayed separately for each cluster/group (which can be chosen in the *Group to visualize* box). It shows on the x-axis the median rank of the feature, and on the y-axis the occurrence (in percentage) of the feature. Each dot represents a feature. The features inside the blue box are defined as specific for the selected cluster/group, while the features inside the red box are defined as not specific (common) for the selected cluster/group.

```{r, out.width = '100%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/congruentfeaturesplotresults.png")
```

The congruent features table shows you the name of the specific features under the *Specific* column, and the name of the non specific (common) features under the *Common* column.

```{r, out.width = '60%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/congruentfeaturestableresults.png")
```

### Venn diagram results

How many features overlap for each cluster/group is shown through Venn diagrams. If you selected more than five comparisons, then the Venn diagrams will not be computed (due to the difficulty in interpreting Venn diagrams when the number of elements is high).

If *Plot* is selected in the top left corner of the main box, the Venn diagram plot will be shown, otherwise if *Table* is selected, the overlap table will be displayed.

The Venn diagram plot is displayed separately for each cluster/group (which can be chosen in the *Group to visualize* box). Each element represents a comparison.

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/venndiagramplotresults.png")
```

The overlap table shows you the content (the feature names) of the overlapping sections of the Venn diagram.

```{r, out.width = '100%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/venndiagramtableresults.png")
```

## Group to visualize

```{r, out.width = '50%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/grouptovisualize.png")
```

**This option is only specific to the congruent features results and Venn diagram results**.

For some type of results, you need to select for which cluster/group you want to visualize the results. You can select the desired cluster/group by using the dropdown menu.

## Customization

```{r, out.width = '50%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/customization.png")
```

The cluster similarity results can be customized in different ways. The respective customization options will appear based on which type of result is being displayed.

Clicking on *Result settings* enables you to change the type of result to visualize. *Plot settings* enables you to change the structure of the plots, and *Color settings* enables you to change the color coding of the plot.

### Result settings

```{r, out.width = '40%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/resultsettings.png")
```

After you click on *Result settings*, you can select which results to show. You can choose from:

- **Dimensionality reduction**.
- **Congruent features**.
- **Venn diagram**.

If you select *Congruent features*, you need to set some additional parameters:

- **Occurrence threshold**: it defines at which point the occurrence (in percentange) is considered enough for defining the specific and common set of features.
- **Common features rank threshold**: the median rank below which a feature could be considered common.
- **Specific features rank threshold**: the median rank below which a feature could be considered specific.

```{r, out.width = '40%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/congruentfeaturessettings.png")
```

### Plot settings (dimensionality reduction)

```{r, out.width = '40%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/dimensionalityreductionplotsettings.png")
```

After you click on *Plot settings*, you can define the following parameters:

- **Dot size**: how big/small the dots will be on the plot.
- **Show grid**: whether to show the background grid or not.
- **Show 3D plot**: whether to show the plot in 3D or not.
- **Hide legend**: whether to hide the legend or not. The legend appears when the plot is color coded by a metadata variable.
- **Put legend at the bottom**: if the legend becomes too big, it will distort the plot. You can put the legend at the bottom to avoid this behaviour.
- **Legend text size**: you can decide how big the legend text should be.

### Color settings (dimensionality reduction)

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/dimensionalityreductioncolorsettings.png")
```

The color settings enable you to select which color coding and symbols to use for clusters/groups and comparisons respectively. After you click on *Color settings*, you have two choices:

- **Colors**: choose the colors for the clusters/groups. See Section \@ref(metadatacoloring) for more information, and Section \@ref(customcoloring) to see how to define custom color schemes.
- **Symbols**: choose the symbols for the comparisons. See Section \@ref(symbols) for more information.

There are also some additional settings to manipulate the colors and symbols:

- **Reverse colors**: whether or not to reverse the color scheme.
- **Reverse symbols**: whether or not to reverse the symbol scheme.
- **Show color picker**: whether or not to show the color picker, to help you define some custom colors. See Section \@ref(customcoloring) for more information.

### Plot settings (congruent features)

```{r, out.width = '40%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/congruentfeaturesplotsettings.png")
```

After you click on *Plot settings*, you can define the following parameters:

- **Dot size**: how big/small the dots will be on the plot.
- **Show grid**: whether to show the background grid or not.

### Color settings (congruent features)

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/clustersimilarity/congruentfeaturescolorsettings.png")
```

After you click on *Color settings*, you can define the following parameters:

- **Common features color**: the color to use to highlight the non specific (common) features in the data.
- **Specific features color**: the color to use to highlight the specific features in the data.
- **Not selected features color**: the color to use to highlight the features that were not selected (that are neither common nor specific).
- **Show color picker**: whether or not to show the color picker, to help you define some custom colors. See Section \@ref(customcoloring) for more information.

## Store and load results

```{r, out.width = '40%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/storeandload/page.png")
```

See Section \@ref(storeandload).

## Exporting the results

```{r, out.width = '40%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/metaanalysis/exporting.png")
```

You can export both the currently visualized plot and table. To download the plot, you can click on the *Export plot* button. To export the table, you can click on the *Export table* button. The corresponding buttons will appear depending on whether you are visualizing the plot or the table.

Check how to export the results by reading Section \@ref(exporting).

## Video tutorial

```{r, echo=FALSE}
embedYoutube("Hte-jwggsEw")
```

## Useful links

- [Distance between sets](http://dx.doi.org/10.1038/234034a0){target="_blank"}
- [Jaccard similarity index meta analysis (method section)](http://www.ncbi.nlm.nih.gov/pubmed/31935371){target="_blank"}