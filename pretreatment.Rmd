# Data pretreatment {#pretreatment}

```{r, out.width = '100%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/pretreatment/page.png")
```

The goal of data pretreatment is to clean the data, so that it becomes amenable for downstream analyses: this step is mandatory and necessary to use all the downstream analyses. Pretreatment encompasses not only filtering, but also data normalization, regression, batch correction, and other techniques that can shape your data in a way that strengthens the biological effects you want to study. The data pretreatment is **specific to each type of data** and **each type of technology** (see Section \@ref(annotationtable)). Each type of pretreatment will be described in this section, divided by type and technology.

```{block, type = 'rmdcaution'}
It is important to note that only data that was annotated as *Raw* can be pretreated. Data that was annotated as *Normalized* or *Corrected* is presumed to be already pretreated. *Normalized* data is assumed to be already filtered, normalized and log transformed, while *Corrected* data is assumed to be batch corrected. If the data was annotated as *Normalized*, you will be only able to perform the batch correction. If the data was annotated as *Corrected*, all the pretreatment options will be disabled.
```

## Normalization and data subset

All data types have some part of the pretreatment which is common. Specifically:

- **Subset of the data based on the metadata**.
- **Normalization of the data** (only for the data that was annotated as *Raw*).

These two steps will be explained in the next subsections. Pretreatment that is not common between data types will be explained in the corresponding sections.

### Subset of the data

The data can be subset at the pretreatment step. This is useful when you want to focus the analyses on a subset of the data. For example, if your data is composed of different organs, you can select a specific organ, so all the pretreatment techniques will be executed on the subset of the data that corresponds to the chosen organ. Alternatevely, it can be used to remove bad batches of observations. For example, if you are analysing *Transcriptomics* data and you know that one batch of samples was sequenced incorrectly, you can exclude those samples (by selecting all the other ones).

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/pretreatment/subset.png")
```

By default, all the observations will be included in the pretreatment processing (by default, the selection is set to *All*). To select a different subset, you first need to select the metadata variable for which you want to subset the data for. After that, you can select the values of the metadata which will define your data subset.

### Normalization type

Normalizing the data is necessary to make all the observations (samples or cells) comparable between each other. For example, in *Transcriptomics* data different samples could be sequenced at different depths (which leads to different library sizes), and we need to correct for that kind of differences to make the samples comparable.

There are three options available to normalize the data:

- **Standard**: this is the normalization specific for each type of data.
- **Log**: a log2 transformation will be applied to the data.
- **None**: no transformation will be applied to the data.

The **Standard** normalization does the following:

- **Transcriptomics (Micro-array)**: log2 transformation (equal to the *Log* setting).
- **Transcriptomics (RNA-seq)**: TMM normalization.
- **Transcriptomics (scRNA-seq)**: Seurat normalization.
- **Metabolomics**: log2 transformation (equal to the *Log* setting).
- **Proteomics**: log2 transformation (equal to the *Log* setting).
- **Cytometry**: arc-sine transformation.

```{block, type = 'rmdimportant'}
This is a crucial step in the analysis pipeline: be sure to select the right normalization for the data you uploaded.
```

## Transcriptomics (Micro-array)

Since the background correction and normalization procedure for micro-array data is platform dependent (Affymetrix, Oligo, Illumina, etc.), you should upload data that has already been background corrected and normalized (e.g. with RMA for Affymetrix micro-arrays). For this reason, there is no special option to pretreat micro-array data, since it should be already pretreated.

## Transcriptomics (RNA-seq)

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/pretreatment/rnaseq.png")
```

For RNA-seq data, an option to filter the low quality features (genes) is provided. To do so, you need to set two parameters:

- **The count per million (CPM) for which a feature is considered expressed**. By default it is 0 (no filtering), but in most cases it could be set to 1.
- **The percentage of observations (samples) that need to express a feature to keep that feature in the data**. The actual number of observations will be also displayed dynamically, as you change the percentage threshold.

The explanation is simple: any feature that is below the CPM threshold will be considered not expressed. After that, we count how many times a feature is expressed in all observations. If the feature is expressed a number of times which is below the chosen threshold, the feature will be filtered out from the data. 

## Transcriptomics (scRNA-seq) {#pretreatmentsc}

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/pretreatment/singlecell.png")
```

For single cell RNA-seq, options to filter the low quality features (genes) and to filter the low quality cells (dead cells) are provided. To do so, you need to set two parameters:

- **The percentage of mitochondrial expression** to remove dead cells.
- **The average expression** for a feature to be considered expressed.

First, the percentage of mitochondrial gene expression is calculated for each cell. The cells that are above the chosen mitochondrial expression threshold are considered low quality cells and filtered out. Usually thresholds from 10% to 5% are used. To avoid the mitochondrial expression filtering altogether, you can set the threshold to 100%.

Similarly, all the features which average (mean) expression is below the defined threshold will be filtered out from the data. By default it is 0 (no filtering),  but usually thresholds from 0.01 to 0.001 are used.

## Metabolomics {#pretreatmentmetabolomics}

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/pretreatment/metabolomics.png")
```

Metabolomics has several options to pretreat the data:

- **You can normalize the data by a reference feature** (metabolite), which you can select from all your available features. This can be useful when spike-ins are used across all your metabolomics samples. *None* is the default selection, which means skipping this step.
- **You can remove features which have too many missing values** (0s are considered as missing values). The number of missing values is counted for each feature: if the counting exceeds the defined threshold for a feature, that feature will be filtered out from the data. If you want to skip this filtering step, you should put the threshold to 100%. Usually thresholds from 5% to 20% are used.
- **The missing values can be imputed by using these different methods** (None, Exclude, Mean, Median, Minimum). Since missing values can make the handling and analysis of the data more arduous and introduce biases, imputation is a good way to avoid listwise deletion. Imputation should be used only if the number of missing values in your data is limited (around 20% per feature). What each method does is explained in Section \@ref(imputation).
- **You can perform a (LOESS) regression on your data**, by using QC samples and the injection order to correct for intensity shifts in your data. An explanation is provided in Section \@ref(regression)

### Imputation {#imputation}

Since missing values can make the handling and analysis of the data more arduous and introduce biases, imputation is a good way to avoid listwise deletion. There are five methods provided to perform imputation on your data:

- **None**: no imputation is performed. Use this setting if you want to skip the imputation of the data.
- **Exclude**: if a feature has at least one missing value, it will be filtered out from the data (listwise deletion).
- **Mean**: for each feature, the missing values will be replaced by the mean abundance of that feature.
- **Median**: for each feature, the missing values will be replaced by the median abundance of that feature.
- **Minimum**: for each feature, the missing values will be replaced by the minimum abundance of that feature.

### Regression {#regression}

The aim of the regression is to correct for intensity shifts in the data, by using the QC samples and injection order. QC samples are pooled samples, which means that they are samples created by taking a small amount of every sample you have, and pool all these amounts together to create a new sample. This is repeated several times, until you have a set of QCs that are effectively equal to each other. These QCs are usually used to assess the efficiency and stability of the mass spectrometer, since the QC samples should all be the same. If there are any differences in the feature abundances between QC samples, then we are going to see technical differences in the data, instead of the biological differences which we are interested in. If we have this situation:

- **Feature X of QC1** taken at time point **1**: 14.7 unit
- **Feature X of QC12** taken at time point **12**: 24.7 unit

Then we need to correct for this difference in abundance, since the abundance of the same feature between all QCs should be the same (equal). This is also why we need the injection order of the samples: we need to know at which time point the samples were injected into the mass spectrometer.

The regression is performed by using LOESS. More information on the LOESS regression can be found in Section \@ref(loess).

To perform the regression, you need to set the following parameters:

- **The LOESS regression model to use**. You can choose between *None*, *Local linear regression* (linear fit), *Local polynomial fit* (polynomial fit) and *Nadaraya-Watson estimator* (local constant fitting). If you select *None*, the regression will be skipped.
- **The metadata variable which contains the values used to identify the QCs**. QCs must be identified with the **QC** keyword in the metadata.
- **The metadata variable which contains the injection order** (should be numeric and starting from 1). The first and last samples to be injected must be QCs. If that is not the case, the regression will not work.
- **The regression span**, which indicates how much data you want to use to perform the local regression at each iteration. 75% mean that 75% of the data is used at each iteration.

Once the pretreatment is performed, you can see the results of the regression by clicking on the *Check regression results* button. A plot will appear, in which you will be able to see the data before regression and the data after regression (for the selected feature).

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/pretreatment/regression.png")
```

The red dots are the QC samples, while the blue dots are the actual samples. As you can see in the plot, there was a downward intensity shift in the data, which was corrected during the regression step. There are no recommended parameters for correcting for the intensity shifts in the data, since the shift is data dependent. Thanks to BIOMEX, you can try to use different parameters, and check the results interactively until you are satisfied with the results.

```{block, type = 'rmdtip'}
You should not overfit the QC samples too much, or the data will become too biased towards the QC samples. You need to find a compromise between not correcting the data and correcting the data too much. If the fitted curve in the *Before* plot passes exactly through all the QCs (or very close), then it could be possible that you are overfitting your data. Try to use a bigger *Span* in that case, or to change the regression model.
```

## Proteomics

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/pretreatment/proteomics.png")
```

The *Proteomics* pretreatment is exactly the same at the *Metabolomics* one. See Section \@ref(pretreatmentmetabolomics) to see how to perform the proteomics pretreatment (that section is referring to metabolites, but it is all the same for proteins).

## Cytometry

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/pretreatment/cyto.png")
```

Cytometry has two options to pretreat the data:

- **You can normalize the data by a reference feature** (protein), which you can select from all your available features. This can be useful when spike-ins are used across all your cytometry samples. *None* is the default selection, which means skipping this step.
- **You can remove features which have too many missing values** (0s are the missing values). The number of missing values is counted for each feature: if the counting exceeds the defined threshold for a feature, that feature will be filtered out from the data. If you want to skip this filtering step, you should put the threshold to 100%. Usually thresholds from 5% to 20% are used.

## Batch correction

Batch effects are technical sources of variation that have been added to the samples during handling. For example if you have too many samples, to label them all at the same time you will have to split the job into manageable rounds of labelling. The samples labelled at the same time will get the same amount of technical varation added to them, but samples labelled at different times may have different amounts of variability added to them. This becomes a major problem when batch effects are confounded with an outcome of interest and lead to incorrect conclusions. Batch correction can be employed to remove these technical differences, bringing out the real biological information present in the data.

Two options are provided to perform batch correction, as for now:

- **None**: the batch correction is not performed (skipped).
- **MNN**: batch correction based on the detection of mutual nearest neighbors (MNNs) in the high-dimensional expression space. This is a single cell RNA-seq batch correction method, which only main requirement is **that at least a subset of the population must be shared between batches**.

You can select the method to use under *Batch correction type*. The batch correction is performed as the last step in the pretreatment.

```{block, type = 'rmdwarning'}
You will not be able to perform supervised analyses (differential analysis, marker set analysis, etc.) if you upload already batch corrected data or if you perform the batch correction during the pretreatment step. To be able to perform the supervised analyses, you must use non-batch corrected normalized data (using the batch as a covariate).
```

### Mutual nearest neighbors (MNN)

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/pretreatment/mnn.png")
```

There are several settings to fill out for this batch correction method:

- **Batch variable**: the variable in the metadata that contains the information about which observation is in which batch (e.g. Batch 1, Batch 2, Batch 3, etc.).
- **Manual ordering**: whether to define in which order to correct the batches. Usually, you want batches that are similar to each other to be corrected first, and the dissimilar batches to be corrected last. If this option is selected, you can define the ordering you desire. If this option is not selected, the ordering will be the ordering in which the batches appear in the metadata.
- **Number of nearest neighbour**: this is the *k* parameter of the nearest neighbour algorithm. It is an integer specifying the number of nearest neighbors to consider when identifying mutual nearest neighbors. The higher it is, the slower the computation of the batch correction will be.
- **Apply cosine transformation before computing distances**: It indicates whether cosine normalization should be performed on the input data prior to calculating distances between cells. This is useful for correcting for scaling differences between observations.
- **Apply cosine transformation before computing corrected expression values**: It indicates whether cosine normalization should be performed prior to computing corrected expression values. If turned off, you obtain the corrected values on the log-scale.
- **Features to use**: which features to use as input in the batch correction algorithm (more on this just below).

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/pretreatment/hvg.png")
```

In the MNN batch correction algorithm, you can choose which features to use to calculate the batch correction. This is useful perform the distance calculation on a subset of genes, e.g., highly variable genes or marker genes. **This may provide more meaningful identification of MNN pairs by reducing the noise from irrelevant genes, and it is actually the recommended way to perform the MNN batch corrrection.** Regardless of the subset of features used to perform the batch correction, **the corrected values are returned for all features.** This is possible as the subset of features is only used to identify the MNN pairs and other cell-based distance calculations. Correction vectors between MNN pairs can then be computed in the original space involving all features in the supplied data.

To understand what the parameters for the highly variable features are, see Section \@ref(highlyvariablefeatures).

```{block, type = 'rmdnote'}
The MNN batch correction can be computationally heavy: if you have tens of thousands of cells in your data, it could take many hours to compute the batch correction.
```

## Perform pretreatment

Once the pretreatment settings have been set-up, you can perform the pretreatment. Click on the *Perform pretreatment* button to perform the pretreatment. After the pretreatment is performed, different entries will appear on the menu on the left side. Those are all the analyses you can perform with BIOMEX.

```{block, type = 'rmdimportant'}
Once you click on the *Perform pretreatment* button, all the downstream analyses will be wiped from the computer memory (not the hard disk), since all those analyses depend on the pretreated data. The same will happen if you click on the *Reset pretreatment* button.
```

```{r, out.width = '50%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/pretreatment/information.png")
```

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/pretreatment/informationtable.png")
```

Information about the pretreatment process can be displayed by clicking on the *Information* button. The table will display all the relevant information about the pretreatment processing, for example how many observations were removed during this step, or how many features were removed during the filtering.

```{block, type = 'rmdtip'}
**It is highly recommended to look at the information table every time you perform the pretreatment**, to make sure the pretreatment was executed as you expected. If you use suboptimal pretreated data during donwstream analysis, you could lose important differences between biological groups, or even worse, get flat out wrong results! 
```

## Video tutorial

```{r, echo=FALSE}
embedYoutube("yTLm5CQPCSw")
embedYoutube("MTUizzFCvkY", message = FALSE)
embedYoutube("d7Uz6pXHjg8", message = FALSE)
embedYoutube("XK5cxVjTQEI", message = FALSE)
embedYoutube("VgVClQb67i4", message = FALSE)
embedYoutube("F7qpdRnzVd8", message = FALSE)
embedYoutube("o6zP-LXVW9M", message = FALSE)
```

## Useful links

- [Transcriptomics data pretreatment (edgeR)](https://www.ncbi.nlm.nih.gov/pubmed/19910308){target="_blank"}
- [Mass spectrometry data pretreatment (NOREVA)](https://www.ncbi.nlm.nih.gov/pubmed/28525573){target="_blank"}
- Single cell transcriptomics data pretreatment: [Seurat](https://www.ncbi.nlm.nih.gov/pubmed/25867923){target="_blank"}, [Scater](https://www.ncbi.nlm.nih.gov/pubmed/28088763){target="_blank"}, [Mutual nearest neighbor batch correction](https://www.ncbi.nlm.nih.gov/pubmed/29608177){target="_blank"}
- [Mass cytometry data pretreatment (CyTOF)](https://www.ncbi.nlm.nih.gov/pubmed/28663787){target="_blank"}
- [Data imputation approaches](https://www.ncbi.nlm.nih.gov/pubmed/29330539){target="_blank"}
