# Header options {#header}

```{r, out.width = '40%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/header/page.png")
```

The header options are located in the top right corner of the page, and these options are available regardless of the page you are currently in. Not all these options are available from the start: all the options will become available after you perform the *Processing* (Section \@ref(processing)) for at least one experiment.

The first button (wrench icon) enables you to set the global settings. The second button (question mark icon) enables you to open this tutorial. The third button (reload icon) enables you to reload the experiment as it was saved. Finally, the last icon (save icon) enables you to save all the results and settings of the currently loaded experiment.

## Global settings {#globalsettings}

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/header/globalsettings.png")
```

Once you click on the *Global settings* icon, you can start to set the global parameters. These are parameters that have an effect throughout the BIOMEX session, regardless of the page you are currently in. You can define the following global parameters:

- **Compress when saving**: by default, BIOMEX saves the data in an uncompressed format. The reason is that it is much faster (up to 20x time faster) to save uncompressed data with respect to compressed data. If you want to save the data in a compressed format, you can select this option. Beware that for big datasets, saving the data in a compressed format can take up to several minutes.
- **Do not use WebGL**: the majority of the plots are displayed by using the WebGL technology, that makes the rendering and exploration of the plot much faster for big datasets. WebGL is not compatible with some browsers or computer configurations: in case the plots are not being rendered properly, you can disable the WebGL technology (at the cost of very slow plots for big datasets).
- **Disable switch experiment confirmation**: when you switch between experiments (Section \@ref(experimentselection)), a confirmation pop-up appears, asking if you want to save the current experiment. That pop-up can be disabled by selecting this option, at the cost of not saving the results of the current experiment when switching to a new experiment (the experiment must be saved manually beforehand in that case, if it is needed).
- **Plotting (factor) limit**: most plots have a limit on the number of factors they can plot, and sometimes on the number of elements they can plot (like for the *Heatmap* analysis plot). This number indicates how many factors/elements you can plot: if the number of factors/elements is greater than this number, the plot will not be displayed.
- **Table (row) limit**: all the interactive Excel tables in BIOMEX have a limit on the number of rows they can have. This number indicates that row limit. If a table has a number of rows that is greater than this number, the table will not be displayed.

```{block, type = 'rmdnote'}
The plot factor limit and the table row limit are present to avoid rendering plots/tables that could slow down or hang BIOMEX altogether. Be careful when you manipulate those settings!
```

## Open manual

By clicking on the *Open manual* icon, you can open the manual directly inside of BIOMEX.

## Reload experiment

By clicking on the *Reload experiment* icon, the current experiment will be reloaded (as it was saved on the hard disk).

```{block, type = 'rmdwarning'}
Reloading the experiment will wipe any new results that have not been saved.
```

## Save experiment

By clicking on the *Save experiment* icon, the results and settings of the currently loaded experiment will be saved on the hard disk, in the output folder indicated in Section \@ref(manageexperiments).

```{block, type = 'rmdtip'}
You should always save the results after a very long or important computation has been completed. Better be safe than sorry!
```

## Video tutorial

```{r, echo=FALSE}
embedYoutube("R8pgMEM_zdk")
```