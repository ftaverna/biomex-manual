# Self-contained set enrichment analysis {#selfcontainedsetenrichmentanalysis}

```{r, out.width = '100%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/selfcontainedsetenrichmentanalysis/page.png")
```

Analyzing large datasets on a feature-by-feature basis makes it difficult to discern global patterns. For example, metabolism can be divided in well-characterized pathways: a powerful approach to study those pathways is to determine whether transcripts, proteins or metabolites in a metabolic pathway are deregulated as a group. Such insight can be derived using the enrichment analysis. The analysis is not restricted to pathways: it can be applied to any possible defined set.

BIOMEX uses ROAST (rotation gene set testing) to perform the self-contained set enrichment analysis. ROAST is a computational method that determines whether an a priori defined set of features shows statistically significant differences between two biological states (e.g. phenotypes). This type of enrichment analysis is called self-contained because when each set is analysed, only the features that belong to the set are considered, not all the features present in the data.

```{block, type = 'rmdimportant'}
**The self-contained set enrichment analysis is based on the results of the differential analysis**. Be sure that the current results of the differential analysis are the ones you want to use as input for the self-contained set enrichment analysis. If no differential analysis is performed yet, you will not be able to perform the self-contained set enrichment analysis.
```

```{block, type = 'rmdwarning'}
You will not be able to perform the supervised analyses if you uploaded already batch corrected data or if you performed the batch correction during the pretreatment step. To be able to perform the supervised analyses, you must use non-batch corrected normalized data (specifying the batch as a covariate).
```

## Design of experiment

```{r, out.width = '50%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/selfcontainedsetenrichmentanalysis/designofexperiment.png")
```

In the self-contained set enrichment analysis design of experiment, you can only choose which sets to use as input for the analysis.

For more information about the design of experiment, see Section \@ref(designofexperiment).

## Settings

```{r, out.width = '50%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/selfcontainedsetenrichmentanalysis/settings.png")
```

In the *Settings* box you will be able to define how to perform the self-contained set enrichment analysis. You can start to define the parameters by clicking on *Method settings*.

### Method settings

```{r, out.width = '40%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/selfcontainedsetenrichmentanalysis/methodsettings.png")
```

When you click on *Method settings*, you can start to define how you want to perform the self-contained set enrichment analysis. You can set the following parameters:

- **Features to use**: which features from the differential analysis to use as input. The choice is between *All* features, *Significant (p-value)* features (p < 0.05) and *Significant (adjusted p-value)* features (p < 0.05).
- **Minimal set size**: it defines the minimal set size. The sets which size is below this threshold will be ignored.
- **Number of rotations**: the number of rotations to perform to assess the statistical significance of each set. Rotations are a Monte Carlo technique for multivariate regression, which can be viewed as analogous to fractional permutations. The higher it is, the more accurate the p-values will be.
- **Random seed**: since the statistical significance is assessed by a rotation test, the same random seed must be used to reproduce the same results using the same parameters.

### Performing the self-contained set enrichment analysis

When the parameters are all set-up, you can click on the *Update* button to compute the self-contained set enrichment analysis results. As soon as the enrichment is computed, a table will appear. The table is interactive and sortable. The columns in the table are:

- **Set**: the name of the set.
- **Number of features**: the number of features in the set.
- **Proportion down**: proportion of features in the set with *z < -sqrt(2)*.
- **Proportion up**: proportion of features in the set with *z > sqrt(2)*.
- **Direction**: whether a set is upregulated (Up) or downregulated (Down) in the experimental group.
- **Pvalue**: the significance of the result. Usually the significance threshold is set at 0.05.
- **Adjusted pvalue**: adjusted p-values calculated with the Benjamini-Hochberg procedure (false discovery rate, FDR).

```{r, out.width = '100%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/selfcontainedsetenrichmentanalysis/tableresults.png")
```

## Customization

```{r, out.width = '50%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/selfcontainedsetenrichmentanalysis/customization.png")
```

The self-contained set enrichment analysis table can be customized in different ways.

Clicking on *Table settings* enables you to select which sets to visualize, and  *Show set content* shows to you the slice of differential analysis relevant to the selected set.

### Table settings

```{r, out.width = '50%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/selfcontainedsetenrichmentanalysis/tablesettings.png")
```

After you click on *Table settings*, you can define the sets to visualize. You can choose between:

- **All**: all the features will be shown.
- **Significant (p-value)**: only the significant features (by p-value) will be shown (p < 0.05).
- **Significant (adjusted p-value)**: only the significant features (by adjusted p-value) will be shown (p < 0.05).

### Show set content

You can obtain the set content for each available set. The set content is the slice of the differential analysis which is relevant to the selected set. To obtain the set content, you first need to select the set of interest by clicking on it on the interactive table. After the click, the selected set should be highlighted in the table.

```{r, out.width = '50%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/selfcontainedsetenrichmentanalysis/tableselection.png")
```

After the set is selected, you can click on *Show set content* to obtain the set content. You can export the set content by clicking on *Export table*. You can read more about how to export a table in Section \@ref(exporting).

```{r, out.width = '80%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/selfcontainedsetenrichmentanalysis/setcontent.png")
```

## Store and load results

```{r, out.width = '40%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/storeandload/page.png")
```

See Section \@ref(storeandload).

## Exporting the results

```{r, out.width = '40%', fig.align = 'center', echo = FALSE}
knitr::include_graphics("png/selfcontainedsetenrichmentanalysis/exporting.png")
```

You can export the enrichment table. To export the table, you can click on the *Export table* button.

Check how to export the results by reading Section \@ref(exporting).

## Video tutorial

```{r, echo=FALSE}
embedYoutube("CiXw1vi9dog")
```

## Useful links

- [Rotation gene set testing (ROAST)](https://www.ncbi.nlm.nih.gov/pubmed/20610611){target="_blank"}
- [Understanding enrichment analysis](https://www.ncbi.nlm.nih.gov/pubmed/17303618){target="_blank"}
- [P-values](https://www.ncbi.nlm.nih.gov/pubmed/20499291){target="_blank"}
- [Adjusted p-values](https://www.ncbi.nlm.nih.gov/pubmed/20010596){target="_blank"}